import 'package:flutter/material.dart';
import 'Carica.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/Sfondo.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
            alignment: AlignmentDirectional.center,
            children: [Sfondo(), _creaCentro(context)]));
  }

  Widget _creaCentro(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(40.0),
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => Carica(),
          ));
        },
        child: Image.asset(
          'lib/assets/logo.png',
          scale: 2,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
