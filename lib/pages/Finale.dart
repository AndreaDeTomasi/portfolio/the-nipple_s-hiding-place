import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/FirebaseManager.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/Sfondo.dart';

class Finale extends StatefulWidget {
  const Finale({required this.firebaseManager});
  final FirebaseManager firebaseManager;

  @override
  _FinaleState createState() => _FinaleState();
}

class _FinaleState extends State<Finale> {
  List<WDim> ris = List<WDim>.empty(growable: true);
  // ignore: unused_field
  late Timer _everyX;

  @override
  void initState() {
    super.initState();

    _everyX = Timer.periodic(Duration(minutes: 5), (timer) {
      setState(() {
        ris.clear();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(alignment: AlignmentDirectional.center, children: [
        Sfondo(),
        FutureBuilder<List<String>>(
            future: widget.firebaseManager.listaCaricamenti(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasData) {
                return _creaSpirale(snapshot.data!);
              }
              return Center(child: CircularProgressIndicator());
            }),
      ]),
    );
  }

  Widget _creaSpirale(List<String> lista) {
    _creaImmagini(lista);
    return StaggeredGridView.countBuilder(
      crossAxisCount: 100,
      itemCount: ris.length,
      itemBuilder: _getImmagine,
      mainAxisSpacing: 4,
      crossAxisSpacing: 4,
      staggeredTileBuilder: _getStaggeredTile,
    );
  }

  Widget _getImmagine(BuildContext context, int index) => ris[index].widget;

  StaggeredTile? _getStaggeredTile(int i) {
    int a = ris[i].pos;
    double x = a / 5;
    if (x < 3) {
      x = 3;
    }
    return StaggeredTile.fit(x.round());
  }

  void _creaImmagini(List<String> lista) {
    List<String> listaR = List.from(lista.reversed);
    for (int a = 0; a < 150 && a < listaR.length; a++) {
      ris.add(WDim(
          widget: Image.network(listaR[a],
              fit: BoxFit.contain, filterQuality: FilterQuality.medium),
          pos: 110 - a));
    }
    ris.shuffle();
  }
}

class WDim {
  WDim({required this.widget, required this.pos});
  final Widget widget;
  final int pos;
}
