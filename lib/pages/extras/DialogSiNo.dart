import 'package:flutter/material.dart';

/// Crea il dialog di avviso con pulsanti Si e No.
///
/// Ritorna [true] se l'utente ha premuto che VUOLE effettuare il logout.
/// Ritorna [null] se l'utente ha dismissato o se ha tappato no.
class DialogSiNo extends StatelessWidget {
  DialogSiNo(
      {required this.testoTitolo,
      required this.testoCorpo,
      this.listview = false});
  final String testoTitolo;
  final String testoCorpo;
  final bool listview;
  @override
  AlertDialog build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: Text(testoTitolo),
      content: Text(testoCorpo),
      actions: [
        IconButton(
            key: const ValueKey('checkButton'),
            icon: Icon(Icons.check, color: Colors.green),
            onPressed: () => Navigator.of(context).pop(true)),
        IconButton(
            key: const ValueKey('closeButton'),
            icon: Icon(Icons.close, color: Colors.red),
            onPressed: () => Navigator.of(context).pop())
      ],
    );
  }
}
