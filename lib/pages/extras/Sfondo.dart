import 'package:flutter/material.dart';

class Sfondo extends StatelessWidget {
  const Sfondo(
      {this.sinistra = const Color.fromARGB(255, 244, 197, 34),
      this.destra = const Color.fromARGB(255, 216, 173, 63)});
  final Color sinistra;
  final Color destra;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
            flex: 1,
            child: Column(
              children: [
                Expanded(child: Container(color: sinistra)),
              ],
            )),
        Flexible(
            flex: 1,
            child: Column(
              children: [
                Expanded(child: Container(color: destra)),
              ],
            ))
      ],
    );
  }
}
