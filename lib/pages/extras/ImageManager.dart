import 'dart:typed_data';
import 'package:image/image.dart' as img;

class ImageManager {
  Uint8List convertiRidimensiona(
      {required Uint8List imageBytes, int quality = 100, int width = 300}) {
    img.Image image = img.decodeImage(imageBytes)!;
    // image = img.copyResize(image, height: -1, width: width);
    image = img.copyResizeCropSquare(image, width);
    List<int> imgEncoded = img.encodeJpg(image, quality: quality);
    return Uint8List.fromList(imgEncoded);
  }
}
