import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/FirebaseManager.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/ImageManager.dart';
import 'package:the_nipple_s_hiding_place/pages/extras/Sfondo.dart';

import 'Finale.dart';
import 'extras/DialogSiNo.dart';

class Carica extends StatefulWidget {
  final TextStyle style =
      const TextStyle(color: Colors.white, fontSize: 30, fontFamily: 'Anton');
  final TextStyle styleSmall =
      const TextStyle(color: Colors.white, fontSize: 20, fontFamily: 'Anton');

  const Carica({Key? key}) : super(key: key);

  @override
  _CaricaState createState() => _CaricaState();
}

class _CaricaState extends State<Carica> {
  Image _image = Image.asset(
    'lib/assets/no-image.png',
    scale: 5,
  );
  Uint8List _imageBytes = Uint8List(0);
  final _picker = ImagePicker();
  bool _caricato = false;
  FirebaseManager _firebaseManager = FirebaseManager();
  ImageManager _imageManager = ImageManager();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
            alignment: AlignmentDirectional.center,
            children: [Sfondo(), _creaCentro()]));
  }

  Future<bool> _caricaFirebase() async {
    Uint8List img8 = _imageManager.convertiRidimensiona(
        imageBytes: _imageBytes, quality: 60, width: 600);
    await _firebaseManager.uploadHQ(img8);
    await _firebaseManager.uploadPic(img8);
    return true;
  }

  /// Pulsante di caricamento foto
  void _caricaPremuto() async {
    bool copyright = await _mostraDialogCopyright();
    if (copyright) {
      bool caricare = await _mostraDialogCaricamento();
      if (caricare) {
        print('carico la foto...');
        showDialog(
            context: context,
            builder: (BuildContext context) => AlertDialog(
                  title: Text('Converting and uploading...'),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('This may take several seconds (up to a minute).'),
                      Container(
                          height: 100,
                          child: Image.asset('lib/assets/clessidra.gif',
                              fit: BoxFit.contain))
                    ],
                  ),
                ));
        await Future.delayed(const Duration(seconds: 1), _caricaFirebase);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => Finale(
                  firebaseManager: _firebaseManager,
                )));
      }
    }
  }

  Widget _creaBordiImmagine(Image image) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(15)),
        border: Border.all(width: 8, color: Colors.white),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: image,
      ),
    );
  }

  Widget _creaCentro() {
    List<Widget> lista = [
      Flexible(
        flex: 10,
        child: _caricato == false
            ? FutureBuilder<String>(
                future: _firebaseManager.percorsoHQ(),
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.done &&
                      snapshot.hasData) {
                    String percorso = snapshot.data!;
                    return _creaBordiImmagine(Image.network(
                      percorso,
                      fit: BoxFit.contain,
                      filterQuality: FilterQuality.medium,
                    ));
                  }
                  return CircularProgressIndicator();
                })
            : _creaBordiImmagine(_image),
      )
    ];
    lista.add(Spacer());
    if (_caricato) {
      lista.add(_creaConferma());
    } else {
      lista.add(Text(
        'This image has been uploaded by another user.',
        textAlign: TextAlign.center,
        style: widget.styleSmall,
      ));
    }
    lista.add(Spacer());
    lista.add(Flexible(
      flex: 7,
      child: _creaPulsanti(),
    ));
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child:
          Column(mainAxisAlignment: MainAxisAlignment.center, children: lista),
    );
  }

  /// Widget al centro, creati dopo che la foto è stata caricata
  Widget _creaConferma() {
    return ElevatedButton(
      onPressed: _caricaPremuto,
      child: Text(
        'UPLOAD PICTURE',
        style: widget.style,
      ),
      style: ElevatedButton.styleFrom(
          onPrimary: Colors.white,
          primary: Colors.purple,
          onSurface: Colors.grey,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10)),
    );
  }

  /// Aggiorna il widget _image con un'immaigne caricata o scattata, in base
  /// a [source].
  Future<void> _creaImmagine({required ImageSource source}) async {
    final pickedFile = await _picker.getImage(source: source, imageQuality: 70);
    if (pickedFile != null) {
      _imageBytes = await pickedFile.readAsBytes();
      setState(() {
        _image = Image.network(pickedFile.path,
            fit: BoxFit.contain, filterQuality: FilterQuality.low);
        _caricato = true;
      });
    } else {
      print('No image selected.');
    }
  }

  /// Crea i pulsanti di caricamento e di scatto.
  Row _creaPulsanti() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Flexible(
          flex: 6,
          child: InkWell(
            onTap: () => _creaImmagine(source: ImageSource.gallery),
            child: Column(
              children: [
                Image.asset(
                  'lib/assets/upload.png',
                ),
                Text(
                  'UPLOAD',
                  style: widget.style,
                  textAlign: TextAlign.center,
                )
              ],
            ),
          ),
        ),
        Spacer(flex: 1),
        Text('OR', style: widget.style, textAlign: TextAlign.center),
        Spacer(flex: 1),
        Flexible(
          flex: 6,
          child: InkWell(
            onTap: () => _creaImmagine(source: ImageSource.camera),
            child: Column(
              children: [
                Image.asset(
                  'lib/assets/scatta.png',
                ),
                Text('TAKE', style: widget.style, textAlign: TextAlign.center)
              ],
            ),
          ),
        )
      ],
    );
  }

  Future<bool> _mostraDialogCaricamento() async {
    bool? ris = await showDialog(
        context: context,
        builder: (BuildContext context) => DialogSiNo(
            testoTitolo: 'Upload picture',
            testoCorpo:
                'Do you really want to upload the selected picture?\nThis cannot be undone.'));
    if (ris == null) {
      return false;
    }
    return ris;
  }

  Future<bool> _mostraDialogCopyright() async {
    bool? ris = await showDialog(
        context: context,
        builder: (BuildContext context) => DialogSiNo(
            testoTitolo: 'Terms',
            testoCorpo:
                'By uploading and applying your Image ("Image"), you warrant and represent that you own or otherwise control all of the rights to your Image, including all the rights necessary for you to provide, post, upload, input or submit the Image, and to transfer all such rights to the owner of this site. In addition to the warranty and representation set forth above, by uploading and posting an Image that contains images, photographs, pictures or that are otherwise graphical in whole or in part (“Images”), you warrant and represent that (a) you are the copyright owner of such Images, or that the copyright owner of such Images has granted you permission to use such Images or any content and/or images contained in such Images consistent with the manner and purpose of your use, and (b) that each person depicted in such Images, if any, has provided consent to the use of the Images, including, by way of example, the distribution, public display and reproduction of such Images. By posting Images, you are granting to all persons who have access to the Images, without compensation, permission to use your Images in connection with the use, including a non-exclusive, world-wide, royalty-free license to: copy, distribute, transmit, publicly display, publicly perform, reproduce, edit, translate and reformat your Images without having your name attached to such Images, and the right to sublicense such rights to third parties.'));
    if (ris == null) {
      return false;
    }
    return ris;
  }
}
