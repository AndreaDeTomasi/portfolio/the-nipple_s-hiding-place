# The nipple's hiding place

Website created for *Installazioni Multimediali* exam, held at *Accademia di Belle Arti di Venezia*.

## Building

To run the project you need to setup [firebase storage](https://firebase.flutter.dev/docs/storage/overview/) and insert your Keys into index.html.

![website screenshot](Screenshot.png)
